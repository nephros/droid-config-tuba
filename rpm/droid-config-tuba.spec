# These and other macros are documented in ../droid-configs-device/droid-configs.inc
# Feel free to cleanup this file by removing comments, once you have memorised them ;)

%define device tuba
%define vendor sony

%define vendor_pretty Sony
%define device_pretty Xperia XA

# Community HW adaptations need this
%define community_adaptation 1

# Sailfish OS is considered to-scale, if in the App Grid you get 4-in-a-row icons,
# and 2-in-a-row or 3-in-a-row app covers in the Home Screen, depending on
# how many apps are open.
# For 4-5.5" device screen sizes of 16:9 ratio, use this formula (hold portrait):
# pixel_ratio = 4.5/DiagonalDisplaySizeInches * HorizontalDisplayResolution/540
# Other screen sizes and ratios will require more trial-and-error.

# Xperia XA: 5.0", 1280x720 --> 4.5/5.0 = 0.9, 720/540=1.33 --> 1.2
# Yet another formula: YourDevicePPI/sbjPPI (245) --> 294/245 --> 1.2
# Xperia XA: 294ppi
%define pixel_ratio 1.2

%define have_modem 1
Provides: ofono-configs

%define ofono_enable_plugins bluez5,hfp_ag_bluez5

%include droid-configs-device/droid-configs.inc
%include patterns/patterns-sailfish-device-adaptation-tuba.inc
%include patterns/patterns-sailfish-device-configuration-tuba.inc

# IMPORTANT if you want to comment out any macros in your .spec, delete the %
# sign, otherwise they will remain defined! E.g.:
#define some_macro "I'll not be defined because I don't have % in front"

